ALTER TABLE `YourBuddy`.`racas`  ADD COLUMN `tipo_id` INT NULL DEFAULT NULL AFTER `nome`;

INSERT INTO racas (nome, tipo_id) values ('Afghan Hound' , 1 ) ;
INSERT INTO racas (nome, tipo_id) values ('Golden retriever' , 1 ) ;
INSERT INTO racas (nome, tipo_id) values ('Akita' , 1 ) ;
INSERT INTO racas (nome, tipo_id) values ('Doberman' , 1 ) ;
INSERT INTO racas (nome, tipo_id) values ('Husky Siberiano' , 1 ) ;
INSERT INTO racas (nome, tipo_id) values ('Beagle' , 1 ) ;
INSERT INTO racas (nome, tipo_id) values ('Chow Chow' , 1 ) ;
INSERT INTO racas (nome, tipo_id) values ('Buldogue Francês' , 1 ) ;
INSERT INTO racas (nome, tipo_id) values ('Shar-pei' , 1 ) ;
INSERT INTO racas (nome, tipo_id) values ('Shiba' , 1 ) ;
INSERT INTO racas (nome, tipo_id) values ('Chihuahua' , 1 ) ;
INSERT INTO racas (nome, tipo_id) values ('Boston terrier' , 1 ) ;
INSERT INTO racas (nome, tipo_id) values ('Pug' , 1 ) ;
INSERT INTO racas (nome, tipo_id) values ('Shih tzu' , 1 ) ;
INSERT INTO racas (nome, tipo_id) values ('Dachshund' , 1 ) ;
INSERT INTO racas (nome, tipo_id) values ('Sem raça definida' , 1 ) ;

INSERT INTO racas (nome, tipo_id) values ('Gato siames', 2);
INSERT INTO racas (nome, tipo_id) values ('Gato angora', 2);
INSERT INTO racas (nome, tipo_id) values ('Persa', 2);
INSERT INTO racas (nome, tipo_id) values ('Himalaio', 2);
INSERT INTO racas (nome, tipo_id) values ('Peterbald', 2);
INSERT INTO racas (nome, tipo_id) values ('Ragdoll', 2);
INSERT INTO racas (nome, tipo_id) values ('Abissínio', 2);
INSERT INTO racas (nome, tipo_id) values ('Cheetah', 2);
INSERT INTO racas (nome, tipo_id) values ('Gato siberiano', 2);
INSERT INTO racas (nome, tipo_id) values ('Chartreux', 2);
INSERT INTO racas (nome, tipo_id) values ('Sem raça definida', 2);

INSERT INTO racas (nome, tipo_id) values ('Calopsita', 3 );
INSERT INTO racas (nome, tipo_id) values ('Papagaio', 3 );
INSERT INTO racas (nome, tipo_id) values ('Periquito Australiano', 3 );
INSERT INTO racas (nome, tipo_id) values ('Catatua branca', 3 );
INSERT INTO racas (nome, tipo_id) values ('Canário da Terra', 3 );
INSERT INTO racas (nome, tipo_id) values ('Sem raça definida', 3 );

INSERT INTO racas (nome, tipo_id ) values ('Beta' , 4 );
INSERT INTO racas (nome, tipo_id ) values ('Peixe Palhaço' , 4 );
INSERT INTO racas (nome, tipo_id ) values ('Imperator' , 4 );
INSERT INTO racas (nome, tipo_id ) values ('Sem raça definida' , 4 );

INSERT INTO racas (nome, tipo_id) values ('Coelho castor rex', 5);
INSERT INTO racas (nome, tipo_id) values ('Coelho hotot', 5);
INSERT INTO racas (nome, tipo_id) values ('Angorá', 5);
INSERT INTO racas (nome, tipo_id) values ('Sem raça definida', 5);


INSERT INTO tipos (nome) values ('Cachorro');
INSERT INTO tipos (nome) values ('Gato');
INSERT INTO tipos (nome) values ('Passáro');
INSERT INTO tipos (nome) values ('Peixe');
INSERT INTO tipos (nome) values ('Coelho');