<?php include "layout/header.php"; ?>
<?php require('function/conexao.php');  ?>
<div class="row  p-3">
    <section class=" bg-white p-2">
        
        <div class="row pt-2">
            <div class="col-md-12">
                <div class="alert alert-warning ">
                    <h2>Afghan Hound</h2>
                </div>
            </div>
            <div class="col-md-2">
                <img src="imagens/alghan-hound.jpg" class="img-thumbnail ">
            </div>
            <div class="col-md-10">
                <h3>Comportamento</h3>
                <p>Afghan Hound é um cachorro bem calmo. Um dos seus piores traços é sua desobediência, mas isso não é um sinal de burrice, ele na maioria das vezes faz o que quer, mas não se preoucupe, pois ele apresenta um baixo nível de dominância e é facilmente adestrado. Convive bem dentro de casa, mas é necessário sair com ele, caso contrário, ele pode ficar estressado. Não é recomendado para quem mora em apartamento.</p>
                <h3>Características</h3>
                <p>O Afghan Hound tem um corpo alto e pelos longod e lisos. Seu peso pode variar de 23Kg e 27Kg. A expectativa de vida é entre 12 e 14 anos, podendo variar de acordo com sua qualidade de vida.</p>
            </div>
        </div>
        <div class="row pt-2">
            <div class="col-md-12">
                <div class="alert alert-warning ">
                <h2>Golden retriever</h2>
                </div>
            </div>
            <div class="col-md-2">
                <img src="imagens/golden-retriever.jpg" class="img-thumbnail ">
            </div>
            <div class="col-md-10">
                <h3>Comportamento</h3>
                <p>Golden retriever é conhecido como um cachorro brincalhão e amigavel. Se destaca pela sua obediência e inteligência, e também adoram brincar, se dão bem com qualquer um e são bem enégeticos, por isso eles necessitam de exercícios diários e não gostam de ficar sozinhos. Não é recomendado para quem mora em apartamento.</p>
                <h3>Características</h3>
                <p>O golden retriever tem um corpo alto e pelagem densa e dourada. Os machos podem ter peso 30-34Kg, e as fêmeas de 25-32Kg. Sua expectativa de vida é de 10 a 12 anos, podendo variar de acordo com sua qualidade de vida. 
                </p>
            </div>
        </div>
        <div class="row pt-2">
            <div class="col-md-12">
                <div class="alert alert-warning ">
                <h2>Akita</h2>
                </div>
            </div>
            <div class="col-md-2">
                <img src="imagens/akita-raca.jpg" class="img-thumbnail ">
            </div>
            <div class="col-md-10">
                <h3>Comportamento</h3>
                <p>Honrando sua herança dos cães do tipo Spitz, o Akita é corajoso, independente, obstinado e tenaz. Afetuoso com sua família, ele é inteiramente devotado e protegerá os membros da casa. Embora não seja uma raça para todas as pessoas, o Akita é um excelente companheiro, quando em boas mãos. O akita adora conviver com a família, mas é recomendado que seja supervisionado quando estiver com as crianças.
                </p>
                <h3>Características</h3>
                <p>O Akita gosta de exercícios físicos e mentais diariamente. Ele precisa de oportunidades para correr em uma área segura ou usando uma coleira em caminhadas mais longas. Com bastante exercício e treinamento, ele pode ser um cachorro doméstico tranquilo e educado. O Akita é mais feliz se puder passar a maior parte do tempo com sua família. Os pelos precisam ser escovados cerca de uma vez por semana para remover pelos mortos, e mais frequentemente durante a perda de pelos.
                Expectativa de vida de 10 a 13 anos, peso entre 32 a 59kg. 
                </p>
            </div>
        </div>
        <div class="row pt-2">
            <div class="col-md-12">
                <div class="alert alert-warning ">
                <h2>Doberman</h2>
                </div>
            </div>
            <div class="col-md-2">
                <img src="imagens/doberman-raca.jpg" class="img-thumbnail ">
            </div>
            <div class="col-md-10">
                <h3>Comportamento</h3>
                <p>Leal, inteligente e com um forte instinto protetor, o doberman é uma das raças mais obedientes e com grande habilidade para aprender e absorver comandos. Além disso, também é muito resistente e veloz. Não é por acaso que esse cachorro se adequa perfeitamente ao trabalho de busca e resgate, policial e serviço militar.
                Em casa, o doberman usa toda a sua imponência e personalidade poderosa para cuidar e defender a sua família, fazendo jus à sua fama de destemido, vigilante e alerta. Mas quando não há situação de perigo, o cachorro dessa raça quer mesmo é brincar e gastar toda a sua energia com passeios, corridas e atividades físicas.</p>
                <h3>Características</h3>
                <p>Esse cachorro de corpo musculoso e aparência elegante possui um temperamento bastante agradável. É muito carinhoso com a sua família, além de educado e obediente, quando bem treinado. Isso torna essa raça muito amorosa e protetora com as crianças da família, desde que os pequenos sejam gentis. Expectativa de vida de 10 a 12 anos e peso entre 27 a 45kg.</p> 
            </div>
        </div>
        <div class="row pt-2">
            <div class="col-md-12">
                <div class="alert alert-warning ">
                    <h2>Husky siberiano</h2>
                </div>
            </div>
            <div class="col-md-2">
                <img src="imagens/husky-siberiano-raca.jpg" class="img-thumbnail ">
            </div>
            <div class="col-md-10">
                <h3>Comportamento</h3>
                <p>O husky siberiano é amigável, gentil e sociável, além de muito trabalhador. Também conhecida como cão de trenó, essa raça foi criada com a companhia de humanos e outros animais para transportar cargas e pessoas, e ter uma convivência pacífica com todos. O cão dessa raça também necessita de exercícios intensos regularmente</p>
                <h3>Características</h3>
                <p>Esse cachorro de corpo musculoso e aparência elegante possui um temperamento bastante agradável. É muito carinhoso com a sua família, além de educado e obediente, quando bem treinado. Isso torna essa raça muito amorosa e protetora com as crianças da família, desde que os pequenos sejam gentis. Expectativa de vida de 10 a 12 anos e peso entre 27 a 45kg.</p> 
            </div>
        </div>
        <div class="row pt-2">
            <div class="col-md-12">
                <div class="alert alert-warning ">
                    <h2>Beagle</h2>
                </div>
            </div>
            <div class="col-md-2">
                <img src="imagens/beagle-raca.jpg" class="img-thumbnail ">
            </div>
            <div class="col-md-10">
                <h3>Comportamento</h3>
                <p>Sua personalidade dócil e aventureira, que adora desbravar o mundo farejando cheiros por toda a parte, como se estivesse seguindo seu instinto, ou melhor, o seu focinho. Beagles são livres e independentes, características comuns em cães de caça. Isso também pode tornar o treinamento um grande desafio, pois soma-se a isso a teimosia. Mas não desista! Mesmo que sejam obedientes apenas por alguns minutos, é preciso persistir para ensiná-los e conseguir controlá-los, diminuindo as chances de que móveis e objetos da casa sofram algum dano.</p>
                <h3>Características</h3>
                <p>O beagle encontra facilmente alimentos e adora comer. Mas, é preciso estar atento, pois ele tem tendência à obesidade. Expectativa de vida de 10 a 15 anos, e peso entre 9 a 13,5kg.</p>
            </div>
        </div>
        <div class="row pt-2">
            <div class="col-md-12">
                <div class="alert alert-warning ">
                    <h2>Chow chow</h2>
                </div>
            </div>
            <div class="col-md-2">
                <img src="imagens/chow-raca.jpg" class="img-thumbnail ">
            </div>
            <div class="col-md-10">
                <h3>Comportamento</h3>
                <p>Reservados, distantes, não muito fãs de carinhos, independentes e teimosos são alguns dos adjetivos que apenas começam a descrever a personalidade do chow chow. Embora seu pelo fofo seja bastante convidativo, eles não gostam muito de ser provocados nem agarrados por crianças e estranhos. É uma raça muito inteligente, sendo facilmente adestrável, mas tem espírito de gato. Portanto, não espere ver aquela agitação e adoração quando chegar em casa.</p>
                <h3>Características</h3>
                <p>Famoso por sua língua azul e pela juba que lembra a de um leão, o chow chow é uma das raças mais exóticas do mundo. Expectativa de vida de 8 a 12 anos, e peso 20 a 32kg</p>
            </div>
        </div>
        <div class="row pt-2">
            <div class="col-md-12">
                <div class="alert alert-warning ">
                    <h2>Buldogue francês</h2>
                </div>
            </div>
            <div class="col-md-2">
                <img src="imagens/buldogue-frances-raca.jpg" class="img-thumbnail ">
            </div>
            <div class="col-md-10">
                <h3>Comportamento</h3>
                <p>Essa raça é muito leal aos donos e, por isso, é uma ótima companhia para quem mora sozinho ou convive com crianças pequenas em casa. O buldogue francês é carinhoso, adora brincar e precisa de poucos exercícios físicos e cuidados. Pode ser um pouco teimoso no início e exigir pulso firme na hora do treinamento, mas, com a dose certa de paciência para entender a rotina da casa, aprende como deve se comportar</p>
                <h3>Características</h3>
                <p>O buldogue francês, conquistou legiões de fãs por sua cara amassada. Expectativa de vida de 12 a 14 anos e peso de 9 a 13kg.</p>
            </div>
        </div>
        <div class="row pt-2">
            <div class="col-md-12">
                <div class="alert alert-warning ">
                    <h2>Shar-pei</h2>
                </div>
            </div>
            <div class="col-md-2">
                <img src="imagens/shar-pei-raca.jpg" class="img-thumbnail ">
            </div>
            <div class="col-md-10">
                <h3>Comportamento</h3>
                <p>Se você estiver procurando por um cachorro calmo, tranquilo e bastante independente, o shar-pei é a raça certa. Mas ele também pode ser bem teimoso! Como é uma raça bastante inteligente, tende a fazer as coisas, digamos… a seu modo. Isso pode fazer com que seja necessário bastante adestramento por parte de seu tutor, a quem o shar-pei deve respeitar, ou ele não vai atender à voz de comando e vai fazer o que bem entender.</p>
                <h3>Características</h3>
                <p>Com sua pele enrugada e língua azul, o shar-pei é uma das raças mais raras do mundo, razão pela qual já figurou entre os recordes do Guiness Book. Expectativa de vida de 8 a 12 anos e peso de 20 a 27kg.</p>
            </div>
        </div>
        <div class="row pt-2">
            <div class="col-md-12">
                <div class="alert alert-warning ">
                    <h2>Shiba</h2>
                </div>
            </div>
            <div class="col-md-2">
                <img src="imagens/shiba-raca.jpg" class="img-thumbnail ">
            </div>
            <div class="col-md-10">
                <h3>Comportamento</h3>
                <p>Também conhecido como shiba inu e pequeno akita, o cachorro dessa raça é um ótimo cachorro de guarda, alerta, ágil e desconfiado com estranhos. Mas, com a família, é carinhoso e muito leal. O shiba tem personalidade forte e precisa de um tutor com pulso firme, pois é um cachorro muito confiante, atrevido e independente. Há quem diga que esse cachorro seja teimoso, mas, na verdade, é um cão de espírito livre, que faz o que deseja e, no fundo, é muito inteligente.</p>
                <h3>Características</h3>
                <p>Com a aparência de uma raposa, musculoso, mas considerado pequeno em comparação aos cachorros da família spitz, o shiba vai te conquistar primeiro com seus pequenos olhos, rosto incrivelmente fofo e pelo macio – depois, pelo seu jeito bem-humorado e brincalhão. Expectativa de vida de 12 a 15 anos e peso de 7 a 11kg.</p>
            </div>
        </div>
        <div class="row pt-2">
            <div class="col-md-12">
                <div class="alert alert-warning ">
                    <h2>Chihuahua</h2>
                </div>
            </div>
            <div class="col-md-2">
                <img src="imagens/chihuahua-raca.jpg" class="img-thumbnail ">
            </div>
            <div class="col-md-10">
                <h3>Comportamento</h3>
                <p>O chihuahua é alerta, gentil e resistente. Seu charme e fofura conquistam a família toda. São cachorros muito devotos e leais ao seu tutor. Os chihuahuas possuem inteligência acima da média, aprendem rápido e trazem em sua personalidade características da família terrier, como autoconfiança e segurança. Cheios de vida e muito animados, cachorros dessa raça também são conhecidos por serem temperamentais e atrevidos. O adestramento pode ajudar a torná-los mais obedientes e parceiros e controlar uma eventual agressividade.</p>
                <h3>Características</h3>
                <p>Os chihuahuas são pequenos no tamanho, mas são grandes no quesito animação. São capazes de surpreender com a sua energia e vontade de brincar, o que os torna cachorros ideais para a família. Expectativa de vida de 12 a 20 anos e peso de 1 a 3kg.</p>
            </div>
        </div>
        <div class="row pt-2">
            <div class="col-md-12">
                <div class="alert alert-warning ">
                    <h2>Boston terrier</h2>
                </div>
            </div>
            <div class="col-md-2">
                <img src="imagens/boston-terrier-raca.jpg" class="img-thumbnail ">
            </div>
            <div class="col-md-10">
            <h3>Comportamento</h3>
                <p>O boston terrier é o verdadeiro sinônimo da palavra amor. Nós sabemos que todas as raças têm seus momentos de estresse, mas esta é diferente. Sabe aquele ser que vibra loucamente quando te vê? Se você queria algo assim, um boston terrier é uma ótima pedida! É amor, carinho, brincadeiras amigáveis e bom senso. Tudo isso num só cachorro. Torna-se realmente complicado procurar defeitos nesta raça. Ela dificilmente se estressa ou se mostra violenta. Aliás, de seu ancestral forte, brigão e imponente, talvez tenha sobrado apenas a história mesmo. O boston terrier se dá bem com todos. Adora crianças, idosos e até estranhos, se estes não ameaçarem o bem-estar de sua família.</p>

                <h3>Características</h3>
                <p>Ser o tutor de um boston terrier é se apaixonar incontáveis vezes durante o dia. Ele é do tamanho ideal para se ter em casa ou no apartamento. Gosta de brincar com todos à sua volta e facilmente se torna o xodó da turma. Expectativa de vida de 13 a 15 anos e peso de 4 a 11kg.</p>
            </div>
        </div>
        <div class="row pt-2">
            <div class="col-md-12">
                <div class="alert alert-warning ">
                    <h2>Pug</h2>
                </div>
            </div>
            <div class="col-md-2">
                <img src="imagens/pug-raca.jpg" class="img-thumbnail ">
            </div>
            <div class="col-md-10">
            <h3>Comportamento</h3>
                <p>Assim como o buldogue francês, a função original do pug é de cão de companhia. Essa raça de grande personalidade é sociável e fiel: está sempre atrás do dono, mesmo quando não é convidado. Essas características fazem dele um cachorro não apropriado para passar longas horas sozinho, porque pode ter ansiedade de separação.</p>

                <h3>Características</h3>
                <p>A raça da cara amassada é do tipo ansiosa para agradar, aprender e amar e vai sempre querer estar por perto. O pug é altamente recomendado para quem mora em apartamento, especialmente por ser sensível às mudanças de temperatura e não exigir muito espaço para queimar energia. Expectativa de vida de 13 a 15 anos e peso de 6 a 8kg.</p>
            </div>
        </div>
        <div class="row pt-2">
            <div class="col-md-12">
                <div class="alert alert-warning ">
                    <h2>Shih tzu</h2>
                </div>
            </div>
            <div class="col-md-2">
                <img src="imagens/shih-tzu-raca.jpg" class="img-thumbnail ">
            </div>
            <div class="col-md-10">
                <h3>Comportamento</h3>
                <p>Charmoso, o shih tzu traz alegria e diversão para a casa. Está sempre disposto a brincar ou a ficar no colo – o importante é estar por perto da família! É um cachorro que costuma latir para as novidades, o que faz dele um verdadeiro cão de alerta. O adestramento pode ajudar a diminuir a frequência dos latidos.</p>

                <h3>Características</h3>
                <p>O olhar dócil, o focinho achatado e o longo pelo sedoso, típicos do shih tzu, são irresistíveis e conquistam fãs há séculos! Brincalhão e companheiro, esse cachorro não precisa de muito espaço para se exercitar e, por isso, vive muito bem em pequenas residências, como apartamentos. Expectativa de vida de 10 a 16 anos e peso de 4 a 7kg.</p>
           </div>
        </div>
            
        <div class="row pt-2">
            <div class="col-md-12">
                <div class="alert alert-warning ">
                    <h2>Dachshund</h2>
                </div>
            </div>
            <div class="col-md-2">
                <img src="imagens/dachshund-raca.jpg" class="img-thumbnail ">
            </div>
            <div class="col-md-10">
                <h3>Comportamento</h3>
                <p>O dachshund é uma raça que traz a própria história em sua personalidade. O passado de caçador é fortemente marcado no temperamento deles. Por isso, eles estão sempre farejando, adoram cavar e enterrar coisas. Também podem ser um pouco teimosos e insistirem em seus “instintos” para fazer as coisas à sua maneira. Essa característica faz com que o adestramento dos linguicinhas quando filhotes tenha melhores resultados do que se realizado mais tarde.</p>

                <h3>Características</h3>
                <p>Com pernas curtas, corpo alongado e orelhas grandes em relação ao seu tamanho, o dachshund tem um formato inconfundível. Frequentemente chamada de “salsicha”. Originalmente usado para a caça de pequenas presas (como texugos, coelhos e raposas), esse cachorro tem um faro apurado, é naturalmente ativo e adora estar com a família. Expectativa de vida de 12 a 16 anos e peso de 7,3 a 15kg.</p> 
            </div>
        </div>
        
    </section>
</div>




<?php include "layout/footer.php"; ?>
