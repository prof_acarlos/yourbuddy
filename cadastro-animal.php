<?php 

include "layout/header.php";
require('function/conexao.php'); 
$connection = getConnection();
$query = 'SELECT * FROM racas';
$result = mysqli_query($connection, $query);
$opcoesRacas = [];
while($row = mysqli_fetch_assoc($result) ) { 
    $row = array_map('utf8_encode', $row);
	$opcoesRacas[$row['tipo_id']][$row['id']] = htmlspecialchars_decode(htmlspecialchars($row['nome']));
}

$query = 'SELECT * FROM tipos';
$result = mysqli_query($connection, $query);
$opcoesTipos = [];
while($row = mysqli_fetch_assoc($result) ) { 
    $row = array_map('utf8_encode', $row);
	$opcoesTipos[$row['id']] = htmlspecialchars_decode(htmlspecialchars($row['nome']));
}


?>
<script>
	
    function optionsAlter(tipo){
        removeOptions(document.getElementById("raca_id"));
        
        
        var todasAsRacas =JSON.stringify(<?php echo json_encode($opcoesRacas) ?> );

        todasAsRacas = JSON.parse(todasAsRacas);
        for(var i in todasAsRacas[tipo]) {
        	addOption(i , todasAsRacas[tipo][i] ) ;
		}

        document.getElementById("raca_id").disabled = false;
    }

    function addOption(key , value) {
        //using the function:
        var option = new Option(value, key);
        var select = document.getElementById("raca_id");
        select.add(option);

    }

    function removeOptions(selectbox)
    {
        var i;
        for(i = selectbox.options.length - 1 ; i >= 0 ; i--)
        {
            selectbox.remove(i);
        }
    }

    function limiteDePalavras(argument) {
    	if(argument.value.substr(-1) == ' '){
    		alert('Apenas uma palavra para descrever seu comportamento.');
    		document.getElementById('comportamento').value = argumen.value.trim();
    	}
    }
    
</script>

<?php if (!isset( $_COOKIE['login'] ) ) : ?>
<script language='javascript' type='text/javascript'>
    alert('Usuário deslogado, por favor faça login');window.location.href='doar.php'
</script>
<?php else : ?>


<div class="row justify-content-md-center pb-3 pt-5" >
	<div class="col-md-4 pt-3 pb-3" style="background-color: #fff">
		<form action="cadastro-animal-inserir.php" method="post" enctype="multipart/form-data">
			<div class="form-group form-login p-3 m-0 " >
				<h2 class="pt-4 pb-2">Cadastro de animal</h2>

                <p>
                	<label for="animal">Animal</label><br>
					<select class="custom-select" name="tipo_id" id="animal"  onchange="optionsAlter(this.value)">
						<option selected value="0">--Escolha--</option>  
						<?php 
						foreach ( $opcoesTipos as $key => $value ){ echo "<option value='$key'>$value</option>"	; } ?>
					</select>
                </p>
				<p>
					<label for="raca">Raça</label><br>
					<select class="custom-select"  name="raca_id" id="raca_id" ></select>
				</p>
                <p><input type="text" class="form-control"  name="nome" id="nome" placeholder="Nome do animal" required></p>
                <p><input type="number" name="idade" class="form-control"  id="idade" placeholder="Idade do animal" required></p>
                <p>
                	<div class="custom-control custom-radio">
					  <input type="radio" name="sexo" id="macho" value="1" required class="custom-control-input">
					  <label class="custom-control-label" for="macho">Macho</label>
					</div>
					<div class="custom-control custom-radio">
					  <input type="radio" name="sexo" id="femea" value="2" class="custom-control-input">
					  <label class="custom-control-label" for="femea">Fêmea</label>
					</div>
                </p>   
                <p>
                    <input type='text' class="form-control" name="comportamento" id="comportamento" oninput="limiteDePalavras(this)" placeholder="Comportamento do animal" required> 
                </p>

               <p>
                	<label for="cidade">Cidade</label><br>
					<select class="custom-select" name="cidade" id="cidade" >
						<option selected value="0">--Escolha--</option>  
	                    <option value="Mogi das Cruzes">Mogi das Cruzes</option>
					</select>
                </p>
                    
                <p>
                    <label for="descricao">Descreva resumidamente seu animal:</label><br>
                    <textarea name="descricao" id="descricao" class="form-control" cols="50" rows="5" required></textarea>
                </p> 

				<p>
					<label for="foto">Foto do seu animal</label>
    				<input type="file" class="form-control-file" id="foto" name="foto">
				</p>
			</div>
			<button type="submite" class="btn btn-block btn-warning btn-submit" >Cadastrar</button>
		</form>
	</div>
</div>


<?php endif; ?>




<?php include "layout/footer.php"; ?>

