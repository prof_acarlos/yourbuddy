<?php


function isActive($menu){
    $url = str_replace(".php", "", $_SERVER["REQUEST_URI"]);
    if($url == $menu)
        echo 'active';
}

?>
<nav class="navbar navbar-expand-md navbar-light bg-dark text-dark " style="background-color: #fbcf3b!important;">
    <a class="navbar-brand text-dark" href="/">YourBuddy</a>
    
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation" >
        <span class="navbar-toggler-icon "></span>
    </button>
    <small >Adote um bichinho, para não ficar sozinho!!!</small>

    <div class="collapse navbar-collapse " id="navbarCollapse">
        <ul class="navbar-nav ml-auto mr-5 text-dark">
            <li class="nav-item <?php isActive('/'); ?>">
                <a class="nav-link text-dark" href="/">Home</a>
            </li>
            <li class="nav-item <?php isActive('/pets'); ?> ">
                <a class="nav-link text-dark" href="pets.php">Pets </a>
            </li>
            <li class="nav-item <?php isActive('/adotar'); ?>">
                <a class="nav-link text-dark" href="adotar.php">Adotar </a>
            </li>
            <li class="nav-item <?php isActive('/doar'); ?>">
                <a class="nav-link text-dark" href="doar.php">Doar </a>
            </li>
            <?php if (isset( $_COOKIE['login'] ) ) : ?>
                <li class="nav-item">
                    <a class="nav-link text-dark"  href="#" onclick="document.getElementById('logout').submit();">Sair</a>
                </li>
            <?php endif ;?>
            
        </ul>
    </div>

</nav>
<form action="logout.php" method='post' id="logout">
    <input type="hidden" name="sair">
</form>