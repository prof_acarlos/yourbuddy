<?php 
include "layout/header.php"; 
require('function/conexao.php'); 

//Consulta no banco de dados se existe esse email com ess senha
$query_animais = "SELECT 
	a.nome, a.idade, a.sexo,a.foto, a.comportamento, a.cidade, a.descricao , 
	b.nome as raca, 
	c.nome as tipo, c.id as tipo_id, 
	d.nome as dono, d.celular
from animais a 
join racas b on a.raca_id = b.id
join tipos c on c.id = a.tipo_id
join usuarios d on d.id = a.usuario_id";

$animais = [];
$animais = mysqli_query($connect, $query_animais);

$todosAnimais = [];

while($row = mysqli_fetch_assoc($animais) ){
	$todosAnimais[$row['tipo_id']][] = $row;
}

?>


	<div class="row pl-3 pt-5 pr-3 pb-5">
		
		<div class="col-md-12 pt-3">
			<?php foreach($todosAnimais as $key => $value  ) { ?>
			<section class=" bg-white p-2 mt-3">
				<div class="col-md-11">
					<h1 class="p2"><?php echo($value[0]['tipo']); ?>s</h1>
				</div>
				<?php foreach($value as $row ) { ?>
					<div class="row pt-2">
			            <div class="col-md-12">
			                <div class="alert alert-warning ">
		                    	<h2><?php echo $row['raca'] ?> </h2>
			                </div>

			            </div>
			            <div class="col-md-2">
			                <img src="<?php echo $row['foto'] ?>" class="img-thumbnail ">
			            </div>
			            <div class="col-md-2">
			                <p><strong>Nome: </strong> <?php echo $row['nome'] ?></p>
			                <p><strong>Idade: </strong> <?php echo $row['idade'] ?></p>
			                <p><strong>Sexo: </strong> <?php echo $row['sexo'] == 1 ? 'Macho' : 'Fêmea' ?></p>
			                <p><strong>Comportamento: </strong> <?php echo $row['comportamento'] ?></p>
			            </div>
			            <div class="col-md-4">
			                <p><strong>Cidade: </strong> <?php echo $row['cidade'] ?></p>
			                <p><strong>Nome do dono: </strong> <?php echo $row['dono'] ?></p>
			                <p><strong>Celular: </strong> <a href="tel:+5511<?php echo $row['celular'] ?>"><?php echo $row['celular'] ?></a></p>
			            </div>
			            <div class="col-md-4">
			                <p><strong>Cidade: </strong> <?php echo $row['descricao'] ?></p>
			            </div>
			        </div>
				<?php } ?>
			</section>	
		<?php }?>
		</div>
	</div>

<?php include "layout/footer.php"; ?>

                    
