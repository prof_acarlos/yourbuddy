<?php include "layout/header.php"; ?>
<?php require('function/conexao.php');  ?>
<div class="row pt-5 pb-5 ">
	<div class="col-md-5 offset-md-1">
		<img src="imagens/cachorro-home.jpg" class="img-fluid rounded float-right img-thumbnail" alt="Imagem responsiva">

	</div>
	<div class="col-md-5 pt-2">
		<h2>Sobre nós</h2>
		<p class="text-justify pt-3">YourBuddy está voltada para a adoção e doação de animais domesticos como por exemplo cachorros, gatos, peixes, passáros e coelhos. O consumidor pode ter acesso a idade, sexo, comportamento, seu antigo dono e a raça do animal. O nosso site também pode te ajudar na hora de escolher a raça do seu animal. Nosso objetivo é ajudar pessoas que gostam de ter uma companhia em casa.</p>
	</div>
</div>

<?php include "layout/footer.php"; ?>
