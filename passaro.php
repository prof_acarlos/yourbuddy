<?php include "layout/header.php"; ?>
<?php require('function/conexao.php');  ?>
<div class="row  p-3">
    <section class=" bg-white p-2">

    <div class="row pt-2">
        <div class="col-md-12">
            <div class="alert alert-warning ">
                <h2>Calopsita</h2>
            </div>
        </div>
        <div class="col-md-2">
            <img src="imagens/passaro-calopsita.jpg" class="img-thumbnail ">
        </div>
        <div class="col-md-10">
            <h3>Comportamento</h3>
            <p>A calopsita é um pássaro muito inteligente, comumente escolhido como pet pela sua habilidade em aprender a falar, cantar e realizar pequenos truques. Ela também é conhecida pela sua capacidade de imitar sons e barulhos repetitivos. Esta espécie possui uma personalidade encantadora e cria laços de afeto muito rapidamente com sua família humana.</p>

            <h3>Características</h3>
            <p>A expectativa de vida de uma calopsita que recebe os cuidados adequados e uma boa alimentação excede o que se espera de um cachorro ou de um gato. Elas vivem por volta de 15 anos e não é incomum que algumas cheguem aos 20 anos de idade.</p>
        </div>
    </div>
    <div class="row pt-2">
        <div class="col-md-12">
            <div class="alert alert-warning ">
                <h2>Papagaio</h2>
            </div>
        </div>
        <div class="col-md-2">
            <img src="imagens/passaro-papagaio.jpg" class="img-thumbnail ">
        </div>
        <div class="col-md-10">
            <h3>Comportamento</h3>
            <p>Papagaios são alegres e engraçados. Uma de suas habilidades é reprosuzir a fala humana, além de terem um facilidade enorme em aprender truques.</p>

            <h3>Características</h3>
            <p>As diferentes espécies de papagaio guardam algumas características comuns. São pássaros médios, atingindo entre 30cm e 40cm. A cor predominante é o verde, e, geralmente, apresentam manchas amarelas, laranjas e azuis. A expectativa de vida do papagaio é grande e pode chegar a mais de 60 anos.</p>
        </div>
    </div>
    <div class="row pt-2">
        <div class="col-md-12">
            <div class="alert alert-warning ">
                <h2>Periquito Australiano</h2>
            </div>
        </div>
        <div class="col-md-2">
            <img src="imagens/periquito-australiano-raca.jpg" class="img-thumbnail ">
        </div>
        <div class="col-md-10">
            <h3>Comportamento</h3>
            <p>É um pássaro geralmente pouco agressivo e que convive em bandos de mais de 60 na natureza, por isso pode ser colocado num mesmo viveiro com outros exemplares. É sociável, curioso, caloroso e gosta de companhia. No caso do animal ser deixado sozinho por longos períodos, é recomendado que tenha pelo menos uma outra ave da mesma espécie na gaiola, para que não fique entediado.</p>

            <h3>Características</h3>
            <p>É a mais popular das raças de estimação. São pequenos e possuem uma coloração muito variada, encontrados em mais de cem diferentes cores. A cor principal da espécie é o verde, mas pode ser azul, cinza, branco, amarelo e combinações entre si. Na cabeça e no dorso, as penas são rajadas em preto. Possui uma inconfundível manchinha azul em cada uma das “bochechas”. Expectativa de vida de 12 a 14 anos e comprimento em média de 18 cm.</p>
        </div>
    </div>
    <div class="row pt-2">
        <div class="col-md-12">
            <div class="alert alert-warning ">
                <h2>Catatua branca</h2>
            </div>
        </div>
        <div class="col-md-2">
            <img src="imagens/catatua-branca-raca.jpg" class="img-thumbnail ">
        </div>
        <div class="col-md-10">
            <h3>Comportamento</h3>
            <p>A situação das Cacatuas Brancas na natureza é considerada vulnerável, principalmente pelo tráfico ilegal. São pássaros muito ativos e ruidosos e, se ficarem encolhidos no poleiro, deve-se averiguar, pois, com certeza, haverá algo de estranho.</p>

            <h3>Características</h3>
            <p>Possuem as penas brancas, contrastando com os olhos, bico e pernas pretos. Uma das características mais marcantes é sua exuberante crista que é erguida ou abaixada quando a ave está excitada ou alarmada. Na maioria dos casos é possível diferenciar o sexo dessa espécie pela cor da íris dos olhos, sendo que a fêmea tem a íris de cor castanho e o macho, preta. Expectativa de vida de 40 anos e comprimento médio de 46 cm.</p>
        </div>
    </div>
    <div class="row pt-2">
        <div class="col-md-12">
            <div class="alert alert-warning ">
                <h2>Canário da Terra</h2>
            </div>
        </div>
        <div class="col-md-2">
            <img src="imagens/canario-da-terra-raca.jpg" class="img-thumbnail ">
        </div>
        <div class="col-md-10">
            <h3>Comportamento</h3>
            <p>Confiável e confiante, quando estão na natureza eles adoram se aproximar das habitações humanas. Além de excelentes cantores, são extremamente valentes e combativos. Estas aves apresentam um canto formado por várias sílabas altas, repetidas, com interrupções no meio e na retomada. As fêmeas também cantam, só que em tom mais baixo. Costumam viver em bandos.</p>

            <h3>Características</h3>
            <p>Estes são pássaros de pequeno porte. Nas partes superiores do corpo a plumagem é cinza-esverdeada, já na parte inferior é amarela com listras acinzentadas. Existe diversificação na plumagem, conforme a região em que habita. No Pantanal, as fêmeas são levemente mais escuras do que os jovens, tendo penas amareladas no corpo, asa e cauda, além das laterais do corpo fortemente riscadas. Já os machos, são de plumagem onde o amarelo é dominante, com tom esverdeado nas partes superiores. Expectativa de vida 20 anos e comprimento médio de 13 cm.</p>
        </div>
    </div>


    </section>
</div>






<?php include "layout/footer.php"; ?>