<?php include "layout/header.php"; ?>
<?php require('function/conexao.php');  ?>
<div class="row  p-3">
    <section class=" bg-white p-2">

    <div class="row pt-2">
        <div class="col-md-12">
            <div class="alert alert-warning ">
                <h2>Beta</h2>
            </div>
        </div>
        <div class="col-md-2">
            <img src="imagens/beta-raca.jpg" class="img-thumbnail ">
        </div>
        <div class="col-md-10">
            <h3>Comportamento</h3>
            <p>Esta espécie é conhecida por ser um peixe de briga. Por isso, não deve ser colocado com outros peixes num mesmo aquário, especialmente se for outro macho beta, pois fica muito agressivo. Recomendado para aquaristas iniciantes, pois não requer muitos cuidados.</p>

            <h3>Características</h3>
            <p>Peixe de água doce, que possui nadadeiras exuberantes e em diferentes formatos, como véu, meia-lua, dupla, entre outras. Pode ser encontrado em diversas cores como amarelo, laranja, magenta e verde, todas brilhantes e intensas. A principal diferenciação entre os sexos é o comprimento das nadadeiras, a da fêmea é mais curta e a do macho bastante longa, que pode se abrir formando um leque. expectativa de vida de 2 a 3 anos e comprimento de 6 a 8 cm.</p>
        </div>
    </div>
    <div class="row pt-2">
        <div class="col-md-12">
            <div class="alert alert-warning ">
                <h2>Peixe Palhaço</h2>
            </div>
        </div>
        <div class="col-md-2">
            <img src="imagens/peixe-palhaco-raca.jpg" class="img-thumbnail ">
        </div>
        <div class="col-md-10">
            <h3>Comportamento</h3>
            <p>Este é um peixe muito bonito e de fácil manutenção. O Peixe-palhaço é sociável e convive bem em grupo e com outras espécies, desde que estas não sejam agressivas. Este peixe habita recifes de corais e é associado geralmente com a anêmona Heteractis Magnifica. É indicado que o Peixe-palhaço seja criado em par, ou seja, que dentro do aquário tenha sempre um macho e uma fêmea. A dupla precisará de espaço e uma série de cuidados especiais com o recipiente para que eles vivam bem e da maneira mais próxima das condições que teriam na vida selvagem.</p>

            <h3>Características</h3>
            <p>Este é um peixe de água salgada. As 30 diferentes espécies do gênero Amphiprion costumam ser encontradas nas regiões de água morna do Oceano Pacífico. Expectativa de vida de 6 anos e comprimento médio de 10 cm.</p>
        </div>
    </div>
    <div class="row pt-2">
        <div class="col-md-12">
            <div class="alert alert-warning ">
                <h2>Imperator</h2>
            </div>
        </div>
        <div class="col-md-2">
            <img src="imagens/imperator-raca.jpg" class="img-thumbnail ">
        </div>
        <div class="col-md-10">
            <h3>Comportamento</h3>
            <p>Este é um peixe relativamente resistente, com algumas exceções. Não é sensível à maioria das alterações na qualidade da água, mas alguns sofrem com altas concentrações de nitratos. Frequentemente é pacífico, mas a espécie maior é normalmente a dominante. Pode ser agressivo com a própria espécie quando o assunto é proteger seu território. Esta espécie precisa de um aquário com, no mínimo, 500 litros, é indicado colocar corais e rochas.</p>

            <h3>Características</h3>
            <p>A principal característica desse peixe vem exatamente na cor, ele muda de cor em sua fase adulta. Quando jovem, tem listras azul marinho, brancas e pretas e a nadadeira transparente. Ao passar à fase adulta, muda para listras amarelas nas nadadeiras e no corpo, no rosto ganha uma faixa preta nos olhos, como se fosse uma máscara. Expectativa de vida de 10 anos e comprimento médio de 30 cm.</p>
        </div>
    </div>

    </section>
</div>






<?php include "layout/footer.php"; ?>