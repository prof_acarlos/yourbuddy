<?php 
include "layout/header.php"; 
require('function/conexao.php'); 
//Consulta no banco de dados se existe esse email com ess senha
$query_animais = "select a.id, a.nome, a.idade, a.sexo,a.foto, a.comportamento, a.cidade, a.descricao , b.nome as raca, c.nome as tipo, d.nome as dono, d.celular
from animais a 
join racas b on a.raca_id = b.id
join tipos c on c.id = a.tipo_id
join usuarios d on d.id = a.usuario_id
where a.usuario_id = %d;";
$animais = [];
if (isset($_COOKIE['id'])){
    $animais = mysqli_query($connect, sprintf($query_animais, $_COOKIE['id']));

}

?>

<?php if (!isset( $_COOKIE['login'] ) ) : ?>
<div class="row justify-content-md-center pb-3 pt-5" >
	<div class="col-md-4 pt-3 pb-3 mt-3 ml-1 mr-1" style="background-color: #fff">
		<form method="POST" action="login.php">

			<div class="form-group form-login pr-5 pl-5 pb-5 m-0" >
				<h5 class="text-center pt-4 pb-5">Login</h5>
				<input type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp" placeholder="E-mail">
				<div class="pt-4"></div>
				<input type="password" class="form-control" id="senha" name="senha" placeholder="Senha">
				
			</div>
			<button type="submit" class="btn btn-primary btn-block btn-login" >Login</button>
		</form>
		
	</div>
	<div class="col-md-4 pt-3 pb-3 mt-3 ml-1 mr-1"  style="background-color: #fff">
		<form method="POST" action="cadastro.php">

			<div class="form-group form-login pr-5 pl-5 pb-5 m-0" style="min-height: 305px;" >
				<h5 class="text-center pt-4 pb-3">cadastro</h5>
				<input type="text" class="form-control" id="nome" name="nome"  placeholder="Nome">
				<div class="pt-2"></div>
				<input type="email" class="form-control" id="email" name="email"  placeholder="E-mail">
				<div class="pt-2"></div>
				<input type="text" class="form-control" id="celular" name="celular"  placeholder="Número de celular">
				<div class="pt-3"></div>
				<input type="password" class="form-control" id="senha" name="senha"  placeholder="Senha">

			</div>
			<button type="submit" class="btn btn-primary btn-block btn-login" >Login</button>
		</form>
	</div>
</div>
<?php else : ?>
	<div class="row pl-3 pt-5 pr-3 pb-5">
		<div class="col-md-11">
			<h3>Animais na doação</h3>
			<a class="btn btn-warning" href="cadastro-animal.php">Cadastrar um animal</a>

		</div>
		<div class="col-md-12 pt-3">
			<?php while($row = mysqli_fetch_assoc($animais) ) { ?>
				<section class=" bg-white p-2 mt-3">
					<div class="row pt-2">
			            <div class="col-md-12">
			                <div class="alert alert-warning ">
								<form action="excluir-animal.php" method="post">
			                    	<h2><?php echo $row['raca'] ?> 
                        				<input type="hidden" name="id" value="<?= $row['id'] ?>">
			                    		<button class="btn btn-sm btn-danger float-right" type="submit">X</button>
			                    	</h2>
	                    		</form>
			                </div>

			            </div>
			            <div class="col-md-2">
			                <img src="<?php echo $row['foto'] ?>" class="img-thumbnail ">
			            </div>
			            <div class="col-md-2">
			                <p><strong>Nome: </strong> <?php echo $row['nome'] ?></p>
			                <p><strong>Idade: </strong> <?php echo $row['idade'] ?></p>
			                <p><strong>Sexo: </strong> <?php echo $row['sexo'] == 1 ? 'Macho' : 'Fêmea' ?></p>
			                <p><strong>Comportamento: </strong> <?php echo $row['comportamento'] ?></p>
			            </div>
			            <div class="col-md-4">
			                <p><strong>Cidade: </strong> <?php echo $row['cidade'] ?></p>
			                <p><strong>Nome do dono: </strong> <?php echo $row['dono'] ?></p>
			                <p><strong>Celular: </strong> <a href="tel:+5511<?php echo $row['celular'] ?>"><?php echo $row['celular'] ?></a></p>
			            </div>
			            <div class="col-md-4">
			                <p><strong>Cidade: </strong> <?php echo $row['descricao'] ?></p>
			            </div>
			        </div>
				</section>
			<?php } ?>
		</div>
	</div>
<?php endif; ?>

<?php include "layout/footer.php"; ?>

                    
