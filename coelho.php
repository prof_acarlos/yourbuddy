<?php include "layout/header.php"; ?>
<?php require('function/conexao.php');  ?>
<div class="row  p-3">
    <section class=" bg-white p-2">

    <div class="row pt-2">
        <div class="col-md-12">
            <div class="alert alert-warning ">
                <h2>Coelho castor rex</h2>
            </div>
        </div>
        <div class="col-md-2">
            <img src="imagens/coelho-castor-rex.jpg" class="img-thumbnail ">
        </div>
        <div class="col-md-10">
            <h3>Comportamento</h3>
            <p>São, no geral, coelhos muito ativos que necessitam que uma família que o deixe correr pela casa em diferentes momentos do dia. Pode proporcionar uma zona sem perigos para ele deixar a sua gaiola aberta. São sociáveis e simpáticos.</p>

            <h3>Características</h3>
            <p>O coelho Castor Rex pode ser de dois tamanhos: o standart, que é geralmente maior, pensado até 5 kg, e a variedade mini que, ao contrário do anterior, pesa entre 1 e 2 kg. Sua pelagem é super suave ao tato.</p>
        </div>
    </div>
    <div class="row pt-2">
        <div class="col-md-12">
            <div class="alert alert-warning ">
                <h2>Coelho hotot</h2>
            </div>
        </div>
        <div class="col-md-2">
            <img src="imagens/coelho-hotot.jpg" class="img-thumbnail ">
        </div>
        <div class="col-md-10">
            <h3>Comportamento</h3>
            <p>São, geralmente, um pouco tímidos mas eventualmente se acostumam à sua presença, deixando que o tutor desfrute de um amigo calmo e suave. Adora comer, o que pode levar à obesidade quando não é devidamente controlado.</p>

            <h3>Características</h3>
            <p>O seu tamanho é muito pequeno, o que faz do animal um exemplar adequado para viver em um apartamento pequeno. No entanto, é importante lembrar que requer espaços para correr e fazer exercício livremente.</p>
        </div>
        <div class="row pt-2">
        <div class="col-md-12">
            <div class="alert alert-warning ">
                <h2>Coelho angorá</h2>
            </div>
        </div>
        <div class="col-md-2">
            <img src="imagens/coelho-angora-raca.jpg" class="img-thumbnail ">
        </div>
        <div class="col-md-10">
            <h3>Comportamento</h3>
            <p>Eles apresentam um temperamento calmo, dócil e adoram ser acariciados pelo seus donos. Tendem a serem mais quietos e tranquilos. Ideal para quem gosta de animais mais sossegados e para quem tem tempo para realizar a escovação necessária.</p>

            <h3>Características</h3>
            <p>O Mini Angorá pode pesar até 2Kg e apresenta um pelo de até 10 cm em todo o corpo inclusive nas orelhas. Seu pelo é macio e sedoso, mas tem a necessidade de ser escovado frequentemente. Pode ter cor única ou até tricolor. Vive de 10 a 12 anos.</p>
        </div>
    </div>

    </section>
</div>








<?php include "layout/footer.php"; ?>