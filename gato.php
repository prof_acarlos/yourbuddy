<?php include "layout/header.php"; ?>
<?php require('function/conexao.php');  ?>
<div class="row  p-3">
    <section class=" bg-white p-2">

    <div class="row pt-2">
        <div class="col-md-12">
            <div class="alert alert-warning ">
                <h2>Gato siames</h2>
            </div>
        </div>
        <div class="col-md-2">
            <img src="imagens/gato-siames.jpg" class="img-thumbnail ">
        </div>
        <div class="col-md-10">
            <h3>Características</h3>
            <p>Os gatos siameses são considerados gatos muito sociáveis e leais. Eles adoram brincar, são muito apegados aos humanos e procuram fazer parte das atividades diárias deles. Siameses adoram adultos, crianças - e conseguem acompanhar bem o ritmo delas, principalmente quando filhotes - e se dão bem com outros animais também. Eles gostam muito de atenção e por isso estão constantemente miando e pedindo por afagos.

            De orelhas pontiagudas e de rosto angular, esse tipo de felino tem o tamanho mediano, um pouco musculoso. Podem viver de 15 a 20 anos com saúde e agilidade, tudo vai depender da sua qualidade de vida.</p>
        </div>
    </div>
    <div class="row pt-2">
        <div class="col-md-12">
            <div class="alert alert-warning ">
                <h2>Gato angora</h2>
            </div>
        </div>
        <div class="col-md-2">
            <img src="imagens/gato-angora.jpg" class="img-thumbnail ">
        </div>
        <div class="col-md-10">
            <h3>Características</h3>
            <p>É um gato dócil e afetuoso, gosta de brincar. Normalmente são sociáveis. Eles são muito inteligentes e não gostam de ficar sozinhos.

            É um gato de porte médio com altura entre 40cm e 45cm, peso entre 3Kg e 5Kg, e expectativa de vida de 12 a 18 anos. Possui pelos longos e lisos que na maioria das vezes é branco.</p>
        </div>
    </div>
    <div class="row pt-2">
        <div class="col-md-12">
            <div class="alert alert-warning ">
                <h2>Persa</h2>
            </div>
        </div>
        <div class="col-md-2">
            <img src="imagens/persa-raca.jpg" class="img-thumbnail ">
        </div>
        <div class="col-md-10">
            <h3>Características</h3>
            <p>Os gatos persas são muito famosos, são os verdadeiros ‘reis’, ou pelo menos é isso que a sua aparência tranquila e majestosa tenta nos passar. O gatinho, que é um dos preferidos das exposições de gatos do Brasil e em todo o mundo, tem a pelagem longa e bem macia. Seu focinho mais plano é o que lhe dá uma carinha de mal, facilmente esquecida quando percebemos a sua doçura ao deitar no colo do dono exclusivamente para receber carinho. Eles são muito carinhosos. Expectativa de vida de 10 a 17 anos e peso de 3,5 a 7kg.</p>
        </div>
    </div>
    <div class="row pt-2">
        <div class="col-md-12">
            <div class="alert alert-warning ">
                <h2>Himalaio</h2>
            </div>
        </div>
        <div class="col-md-2">
            <img src="imagens/himalaio-raca.jpg" class="img-thumbnail ">
        </div>
        <div class="col-md-10">
            <h3>Características</h3>
            <p>O gato himalaia é uma raça de gatos razoavelmente nova no Brasil e no mundo. Em 1950 o gato siamês cruzou com o gato persa para criar uma raça com o corpo do persa mas com a coloração do gato siamês, sendo este nomeado de Himalaio. O resultado foi corpo peludo do persa e cores únicas do siamês. Expectativa de vida de 9 a 15 anos e peso de 4 a 6kg.</p>
        </div>
    </div>
    <div class="row pt-2">
        <div class="col-md-12">
            <div class="alert alert-warning ">
                <h2>Peterbald</h2>
            </div>
        </div>
        <div class="col-md-2">
            <img src="imagens/peterbald-raca.jpg" class="img-thumbnail ">
        </div>
        <div class="col-md-10">
            <h3>Características</h3>
            <p>Raça de gatos do tipo oriental. É um gato muito saudável, apresenta ossos finos, corpo e pés alongados. Não possui pelos e sua pele tem uma característica importante: é delicada e ao mesmo tempo elástica, permitindo grandes movimentações. A cauda é bem longa e forte. Em algumas partes, como focinho, patas e orelha, a cor é um pouco diferente do resto do corpo. Expectativa de vida de 12 a 15 anos e 2,4 a 4,5kg.</p>
        </div>
    </div>
    <div class="row pt-2">
        <div class="col-md-12">
            <div class="alert alert-warning ">
                <h2>Ragdoll</h2>
            </div>
        </div>
        <div class="col-md-2">
            <img src="imagens/ragdoll-raca.jpg" class="img-thumbnail ">
        </div>
        <div class="col-md-10">
            <h3>Características</h3>
            <p>É um gato grande, uma das maiores raças. Possui pelo longo e abundante. Os olhos são azuis e cativantes. A cor da pelagem pode ser cinza-azulado, chocolate, lilás, canela, castanho avermelhado e creme. Apesar de mais raro, também pode ser encontrado malhado. É um gato afetuoso, relaxado, feliz, carinhoso, calmo, inteligente e se dá bem com crianças e outros animais, incluindo cães. Não é indicado para pessoas que procuram um gato que interaja com o dono ou cheio de energia para brincar. Expectativa de vida de 12 a 17 anos e peso de 4,5 a 9kg.</p>
        </div>
    </div>
    <div class="row pt-2">
        <div class="col-md-12">
            <div class="alert alert-warning ">
                <h2>Abissínio</h2>
            </div>
        </div>
        <div class="col-md-2">
            <img src="imagens/abissinio-raca.jpg" class="img-thumbnail ">
        </div>
        <div class="col-md-10">
            <h3>Características</h3>
            <p>Corpo longo, leve, gracioso e musculoso, solidamente construído, com leve arco nas costas. Cabeça em forma de cunha modificada e com contornos arredondados. De perfil, o aumento da ponte do nariz para a frente, junto da crista da testa, mantém a aparência selvagem. Orelhas grandes e arqueadas para a frente, em alertaOlhos grandes, amendoados, delineados em cor escura e de cor âmbar ou verde. Focinho arredondado. Pernas magras, longas e musculosas. Expectativa de vida de 9 a 15 anos.</p>
        </div>
    </div>
    <div class="row pt-2">
        <div class="col-md-12">
            <div class="alert alert-warning ">
                <h2>Cheetah</h2>
            </div>
        </div>
        <div class="col-md-2">
            <img src="imagens/cheetah-raca.jpg" class="img-thumbnail ">
        </div>
        <div class="col-md-10">
            <h3>Características</h3>
            <p>É um gato sociável, inteligente, dócil e gentil com as pessoas. Apesar de sua descendência selvagem, não é agressivo e pode conviver tranquilamente com crianças. As fêmeas desta raça costumam ter comportamento maternal em relação a outros gatos e pequenos animais, o mesmo vale para os machos. Gato de porte médio a grande, pelo curto e musculoso. A cor da pelagem é malhada e pode ser encontrada nas combinações preto e marrom, marrom e bege, canela e dourado. O formato dos olhos e das orelhas, ambos grandes, potencializam o aspecto selvagem. Expectativade vida de 10 a 15 anos e peso de 6,5 a 10kg.</p>
        </div>
    </div>
    <div class="row pt-2">
        <div class="col-md-12">
            <div class="alert alert-warning ">
                <h2>Gato siberiano</h2>
            </div>
        </div>
        <div class="col-md-2">
            <img src="imagens/siberiano-raca.jpg" class="img-thumbnail ">
        </div>
        <div class="col-md-10">
            <h3>Características</h3>
            <p>Um gato como este é simpático e sem medo de desfrutar da companhia de uma vasta gama de animais de estimação, mas procura e gosta mesmo é da interação com humanos. Carinhoso e inteligente, gentil e calmo, este é o Siberiano. Este animal também se dá bem com crianças. Expectativa de vida de 11 a 15 anos e peso de 3,6 a 7,2kg.</p>
        </div>
    </div>
    <div class="row pt-2">
        <div class="col-md-12">
            <div class="alert alert-warning ">
                <h2>Chartreux</h2>
            </div>
        </div>
        <div class="col-md-2">
            <img src="imagens/chartreux-raca.jpg" class="img-thumbnail ">
        </div>
        <div class="col-md-10">
            <h3>Características</h3>
            <p>Gato de médio a grande porte. Seu corpo é robusto, com uma densa massa muscular. Seu pelo é curto e grosso, e o tamanho vai do médio ao curto, ligeiramente lanudo na textura e com uma cor uniforme desde a raiz, normalmente um azul acinzentado. Seus olhos são expressivos, arredondados e abertos. A cor vai do cobre ao ouro. Expectativa de vida de 11 a 15 anos e peso de 3 a 7kg.</p>
        </div>
    </div>

    </section>
</div>







<?php include "layout/footer.php"; ?>